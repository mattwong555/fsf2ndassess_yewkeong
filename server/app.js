var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var Sequelize = require('sequelize');

const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "qwerty";

var conn = new Sequelize(
    'grocery_list',
    MYSQL_USERNAME,
    MYSQL_PASSWORD, {
        host: 'localhost',
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// import the database models into the app.js
var grocery_list = require('./models/grocery_list')(conn, Sequelize);




const NODE_PORT = process.env.NODE_PORT || 8080;

// TODO check the dirname is correct when test .
const CLIENT_FOLDER = path.join(__dirname, '../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, 'assets/messages');

// const API_DEPARTMENTS_ENDPOINT = "/api/departments";

var app = express();




// setup of the configuration of express.
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/api/brand", function(req, res) {
    console.log(req.query.searchString);
    grocery_list
        .findAll({
            where: {
                $or: [
                    { brand: { $like: "%" + req.query.searchString + "%" } },
                    { name: { $like: "%" + req.query.searchString + "%" } }
                ]

            },
            limit: 20

        })
        .then(function(grocery) {
            res.status(200).json(grocery);
            console.log(grocery)
        }).catch(function(err) {
            res.status(500).json(err);
        });
});

app.get("/api/name", function(req, res) {
    console.log(req.query.searchString);
    grocery_list
        .findAll({
            where: {
                $or: [
                    { brand: { $like: "%" + req.query.searchString + "%" } },
                    { name: { $like: "%" + req.query.searchString + "%" } }
                ]
            },
            limit: 20
        })
        .then(function(grocery) {
            res.status(200).json(grocery);
            console.log(grocery)
        }).catch(function(err) {
            res.status(500).json(err);
        });
});

app.get("/api/product/:productId", function(req, res) {
    console.log(req.params.productId);
    grocery_list
        .findOne({
            where: {
                id: req.params.productId
            },
        })
        .then(function(product) {
            res.status(200).json(product);
            console.log(product)
        }).catch(function(err) {
            res.status(500).json(err);
        });
});


app.put("/api/products/:productId", function(req, res) {
    grocery_list
        .find({
            where: {
                id: Number(req.params.productId)
            }
        }).then(function(result) {
            result.updateAttribute({
                brand: req.body.brand,
                name: req.body.name,
                upc12: req.body.upc12
            }).catch(function(err) {
                res.status(500).json(err);
            });
        })
});

app.use(function(req, res) {
    res.status(400).sendFile(path.join(MSG_FOLDER, "404.html"));
});

app.use(function(err, req, res, next) {
    console.log("An error had occured 500");
    res.status(500).sendFile(path.join(MSG_FOLDER, "500.html"));
});


app.listen(NODE_PORT, function() {
    console.log("Server is running at port " + NODE_PORT);
})