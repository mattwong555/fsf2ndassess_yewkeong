(function() {
    angular
        .module("DMS")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['$window', 'DeptService'];

    function SearchDBCtrl($window, DeptService) {
        var vm = this;

        vm.searchString = '';
        vm.result = null;
        vm.showManager = false;


        vm.search = search;
        vm.searchForName = searchForName;
        vm.product = {};
        vm.editBrand = "";
        vm.editName = "";
        vm.editBarcode = 0;



        function search() {
            vm.showManager = false;
            DeptService

                .retrieveDB(vm.searchString)
                // console.log(vm.searchString)
                .then(function(results) {
                    vm.product = results.data;
                    console.log(vm.product);
                    console.log(vm.product.id);
                })
                .catch(function(err) {
                    console.log("error " + err);
                });
        }

        function searchForName() {
            vm.showManager = true;
            DeptService

                .retrieveName(vm.searchString)
                .then(function(results) {
                    vm.product = results.data;
                    console.log(vm.product);
                })
                .catch(function(err) {

                    console.info("error " + JSON.stringify(err));
                });
        }

        //Idea at 1pm , use windows.location.assign to pop up edit.html but FAILED item not inside edit .html so i cant PUT it back
        // when client click Edit on searchDB.html, it will call this function which will init details and also windows.location.assign to edit.html 


        // function initDetails() {
        //     console.log("-- show.controller.js > initDetails()");

        //     vm.product.brand = vm.editBrand; //trying to bring the variables that is stored in global after the get and store into new var to populate the edit.html
        //     vm.product.name = vm.editName;
        //     vm.product.upc12 = vm.editBarcode;

        //     $window.location.assign("/app/edit/edit.html");
        // }





    }
})();