(function() {
    angular
        .module("DMS")
        .service("DeptService", DeptService);

    DeptService.$inject = ['$http'];

    function DeptService($http) {
        var service = this;

        service.retrieveDB = retrieveDB;
        service.retrieveName = retrieveName;
        service.updateProduct = updateProduct;
        service.show = show;

        // for searching brand 
        function retrieveDB(searchString) {
            return $http({
                method: 'GET',
                url: "/api/brand",
                params: { 'searchString': searchString }

            });
            console.log(params)
        }

        //for searching name
        function retrieveName(searchString) {
            return $http({
                method: 'GET',
                url: "/api/name",
                params: { 'searchString': searchString }
            });
            console.log(params)
        }

        //for editing 1 item on edit.html
        function show() {
            return $http({
                method: 'GET',
                url: "/api/product" + productId,
            });

        }


        //for editing of products
        function updateProduct() {
            $http({
                method: 'PUT',
                url: "/api/products/" + product.Id,
                data: {
                    brand: brand,
                    name: name,
                    upc12: upc12
                }
            });
        }



    }
})();